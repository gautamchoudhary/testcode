/**
 * Copyright 2017, Google, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

// [START gae_node_request_example]
const express = require('express');
var https = require('https');
var fs = require('fs');
var request = require('request-promise');
var bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
const username= 'WBI2PI';
const password ='JZ{8h@bJFg';
//const url =  "https://integration-dmw.ea.holcim.net:443/RESTAdapter/WBI/MRGeneration" // development
const url =  "https://integration-bmw.ea.holcim.net:443/RESTAdapter/WBI/MRGeneration" // Quality
//const url =  "https://integration-pmw.ea.holcim.net:443/RESTAdapter/WBI/MRGeneration" // Production

const auth = {
  'user': username,
  'pass': password,
};
var options = {
  key: fs.readFileSync( './localhost.key' ),
  cert: fs.readFileSync( './localhost.cert' ),
  requestCert: false,
  rejectUnauthorized: false
};



app.get('/', (req, res) => {
  res
    .status(200)
    .send('Hello, world!')
    .end();
});

app.get('/test', (req, res) => {
  res
    .status(200)
    .json({apimessage:'connected successfully'})
    .end();
});
app.post('/hanover', (req, res) => {
    console.log(JSON.stringify(req.body ) + "Request Body - Gautam");
   // res.send('POST request to the homepage')
    //res.send(req.body);
     let record = new Object();
    //console.log(req.body);
     record = JSON.parse(req.body["record"]);
     record = {record};
     console.log(JSON.stringify(record) + "requestBody after formating");
   request.post({url: url, headers: {'Content-type': 'application/json'}, auth: auth, body:record , json: true},function (error, response, body) {
    if(error)
    {
      console.error("Error while communication with api and ERROR is :  " + error);
       res.send(error);
     }
     console.log("response-> from SAP ", JSON.stringify(response));
     //console.log("resp.notificationNumber" + body.record.notificationNumber);
     res.send(JSON.stringify(response.body.MRGenerationRes.record));
     res.end();
    })
  // res.status(301).redirect("https://www.google.com").end();

})

// Start the server
//const PORT = process.env.PORT || 8080;
var port = process.env.PORT || 443;
var server = https.createServer( options, app );

server.listen( port, function () {
    console.log( 'Express server listening on port ' + server.address().port );
} );
// [END gae_node_request_example]

module.exports = app;
